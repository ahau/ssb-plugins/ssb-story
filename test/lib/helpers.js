
const { promisify: p } = require('util')

function SaveCollection (server) {
  return function saveCollection (input) {
    return save(server.story.collection.create, input)
  }
}

function SaveStory (server) {
  return function saveStory (input) {
    return save(server.story.create, input)
  }
}

function SaveLink (server) {
  return function saveLink (input) {
    return save(server.story.collection.createStoryLink, input, 'collection-story')
  }
}

function save (saveFunction, input, type) {
  input.authors = { add: ['*'] }
  return p(saveFunction)(type || 'test', input)
}
module.exports = {
  SaveStory,
  SaveCollection,
  SaveLink
}
