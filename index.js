const API = require('./method')

module.exports = {
  name: 'story',
  version: require('./package.json').version,
  manifest: {
    create: 'async',
    get: 'async',
    update: 'async',
    collection: {
      create: 'async',
      get: 'async',
      update: 'async'
    }
  },
  init: (server) => {
    return API(server)
  }
}
