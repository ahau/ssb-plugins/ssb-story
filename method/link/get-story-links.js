const { isMsg } = require('ssb-ref')
const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const { where, type, toPullStream } = require('ssb-db2/operators')
const { COLLECTION_STORY_LINK } = require('../../lib/constants')

module.exports = function GetStoryLinks (server, loadCrut) {
  const getActiveLink = (linkId, cb) => {
    server.story.collection.getStoryLink(linkId, (err, link) => {
      if (err) return cb(null, null)

      if (link.tombstone) return cb(null, null)

      if (link.states.length && link.states.every(state => state.tombstone)) return cb(null, null)
      // NOTE "null" link means no link / tombstoned

      cb(null, link)
    })
  }

  const crut = loadCrut(COLLECTION_STORY_LINK)

  return function getStoryLinks ({ collectionId, storyId }, cb) {
    if (!collectionId && !storyId) return cb(new Error('getStoryLinks() needs a collection or story id'))

    // build the filter based on input
    const filter = {}

    if (collectionId) {
      if (!isMsg(collectionId)) return cb(new Error('The collectionId given to getStoryLinks() is not a valid'))
      filter.parent = collectionId
    }
    if (storyId) {
      if (!isMsg(storyId)) return cb(new Error('The storyId given to getStoryLinks() is not a valid'))
      filter.child = storyId
    }

    pull(
      server.db.query(
        where(type(COLLECTION_STORY_LINK)),
        toPullStream()
      ),
      pull.filter(msg => filter.parent ? (msg.value.content.parent === filter.parent) : true),
      pull.filter(msg => filter.child ? (msg.value.content.child === filter.child) : true),
      pull.filter(crut.spec.isRoot),
      pull.map(link => link.key),
      paraMap(getActiveLink, 4),
      pull.filter(Boolean),
      pull.collect((err, results) => {
        if (err) return cb(err)

        cb(null, results)
      })
    )
  }
}
