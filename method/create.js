module.exports = function Create (server, findOrCreateCrut, superType) {
  return function create (subType, input, cb) {
    const crut = findOrCreateCrut(`${superType}/${subType}`)

    crut.create(input, cb)
  }
}
